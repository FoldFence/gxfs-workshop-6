from flask import Flask, jsonify

app = Flask(__name__)

did_document_stuttgart = {
    "@context": [
      "https://www.w3.org/ns/did/v1",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "id": "did:web:didunistuttgart",
    "verificationMethod": [
      {
        "id": "did:web:didunistuttgart#owner",
        "type": "JsonWebKey2020",
        "controller": "did:web:didunistuttgart",
        "publicKeyJwk": {
          "kty": "OKP",
          "crv": "Ed25519",
          "x": "yaHbNw6nj4Pn3nGPHyyTqP-QHXYNJIpkA37PrIOND4c"
        }
      }
    ],
    "service": [
      {
        "id": "did:web:didunistuttgart#gx-trust-list-issuer-uni-stuttgart",
        "type": "gx-trust-list-issuer",
        "serviceEndpoint": "http://fed1-tfm:16003/tspa-service/tspa/v1/uni_stuttgart.federation1.train/vc/trust-list"
      }
    ],
    "authentication": [
      "did:web:didunistuttgart#owner"
    ],
    "assertionMethod": [
      "did:web:didunistuttgart#owner"
    ]
  }

did_document_tallinn = {
    "@context": [
      "https://www.w3.org/ns/did/v1",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "id": "did:web:didunitallinn",
    "verificationMethod": [
      {
        "id": "did:web:didunitallinn#owner",
        "type": "JsonWebKey2020",
        "controller": "did:web:didunitallinn",
        "publicKeyJwk": {
          "kty": "OKP",
          "crv": "Ed25519",
          "x": "yaHbNw6nj4Pn3nGPHyyTqP-QHXYNJIpkA37PrIOND4c"
        }
      }
    ],
    "service": [
      {
        "id": "did:web:didunitallinn#gx-trust-list-issuer-uni-tallinn",
        "type": "gx-trust-list-issuer",
        "serviceEndpoint": "http://fed1-tfm:16003/tspa-service/tspa/v1/uni_stuttgart.federation1.train/vc/trust-list"
      }
    ],
    "authentication": [
      "did:web:didunistuttgart#owner"
    ],
    "assertionMethod": [
      "did:web:didunistuttgart#owner"
    ]
  }

did_document_student = {
    "@context": [
      "https://www.w3.org/ns/did/v1",
      "https://w3id.org/security/suites/jws-2020/v1"
    ],
    "id": "did:web:didStudent",
    "verificationMethod": [
      {
        "id": "did:web:didStudent#owner",
        "type": "JsonWebKey2020",
        "controller": "did:web:didStudent",
        "publicKeyJwk": {
          "kty": "OKP",
          "crv": "Ed25519",
          "x": "yaHbNw6nj4Pn3nGPHyyTqP-QHXYNJIpkA37PrIOND4c"
        }
      }
    ],
    "authentication": [
      "did:web:didStudent#owner"
    ],
    "assertionMethod": [
      "did:web:didStudent#owner"
    ]
  }

@app.route('/unistuttgart/did.json')
def get_did_document_stuttgart():
    return jsonify(did_document_stuttgart)

@app.route('/unitallinn/did.json')
def get_did_document_tallinn():
    return jsonify(did_document_tallinn)

@app.route('/unistudent/did.json')
def get_did_document_student():
    return jsonify(did_document_student)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=10000)

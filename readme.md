


# Build Docker Image

```
docker build -t did-document-server .
```

# Run

```
docker run --rm --network=train --name=did-document-server  -p 10000:10000 did-document-server
```